<div>
    <div
        wire:ignore id="map"
        style="width:100%;height:500px;">
    </div>

    <form wire:submit.prevent="storePoint"
        class="flex w-1/2 mt-6 mx-auto justify-center items-start"
        autocomplete="off"
    >
        <div class="w-full">
            <input type="text"
                   wire:model="coordinates"
                   class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                   placeholder="Enter your coordinates..."
                   required
            >

            @error('coordinates')
                <p class="text-red-500 text-xs mt-2 italic">{{ $message }}</p>
            @enderror

            <p class="text-gray-500 text-xs mt-2">Example: 50.2269462, 27.67385669</p>
        </div>

        <button type="submit" class="text-white ml-4 bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Pin</button>
    </form>
</div>

<script>
    const markers = new Map();
    let map;
    const points = @js($points);
    let defaultLat = @js($defaultLat);
    let defaultLng = @js($defaultLng);
    const markerLifeTime = @js($markerLifeTime) * 1000;

    if (points.length) {
        defaultLat = points[0].lat;
        defaultLng = points[0].lng;
    }

    (g=>{var h,a,k,p="The Google Maps JavaScript API",c="google",l="importLibrary",q="__ib__",m=document,b=window;b=b[c]||(b[c]={});var d=b.maps||(b.maps={}),r=new Set,e=new URLSearchParams,u=()=>h||(h=new Promise(async(f,n)=>{await (a=m.createElement("script"));e.set("libraries",[...r]+"");for(k in g)e.set(k.replace(/[A-Z]/g,t=>"_"+t[0].toLowerCase()),g[k]);e.set("callback",c+".maps."+q);a.src=`https://maps.${c}apis.com/maps/api/js?`+e;d[q]=f;a.onerror=()=>h=n(Error(p+" could not load."));a.nonce=m.querySelector("script[nonce]")?.nonce||"";m.head.append(a)}));d[l]?console.warn(p+" only loads once. Ignoring:",g):d[l]=(f,...n)=>r.add(f)&&u().then(()=>d[l](f,...n))})({
        key: @js($googleMapsApiKey),
        v: "weekly",
        language: "en",
        region: "UA",
    });

    async function initMap() {
        const { Map } = await google.maps.importLibrary("maps");
        map = new Map(document.getElementById("map"), {
            zoom: 6,
            center: { lat: defaultLat, lng: defaultLng },
        });

        points.forEach((p) => {
            addMarker(p);
        });
    }

    window.onload = function () {
        Echo.channel(`points`)
            .listen('PointAdded', (e) => {
                addMarker(e.point);
            });
    };

    document.addEventListener('livewire:load', function () {
        initMap();
    });

    window.addEventListener('addedNewMarker',function(e){
        addMarker(e.detail);

        const coord = new google.maps.LatLng(e.detail.lat, e.detail.lng);
        map.setCenter( coord );
    });

    function addMarker(coords) {
        if (!markers.has(coords.id)) {
            const marker = new google.maps.Marker({
                position: coords,
                animation: google.maps.Animation.DROP,
            });

            marker.setMap(map);
            markers.set(coords.id, marker);

            removeMarkerAfterExpired(coords);
        }
    }

    function removeMarkerAfterExpired(coords) {
        const createdAtTime = Date.parse(coords.created_at);
        const currentTime = Date.now();
        const diffTime = markerLifeTime - Math.abs(currentTime - createdAtTime);

        setTimeout(() => {
            markers.get(coords.id).setMap(null);
            markers.delete(coords.id);
        }, diffTime);
    }
</script>
