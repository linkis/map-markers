<?php
namespace App\Services;

use App\Models\Point;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class PointService
{
    public function getAllPointsForLastSeconds(int $seconds): Collection
    {
        return Point::where('created_at', '>',
            Carbon::now()->subSeconds($seconds)->toDateTimeString()
        )->get();
    }

    public function store(array $data): Point
    {
        return Point::create($data);
    }
}
