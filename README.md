## Map Markers app

### Requirements

- PHP 8
- Docker

### Installation
````
git clone git@gitlab.com:linkis/map-markers.git
cd map-markers

cp .env.example .env
composer install

./vendor/bin/sail up -d
./vendor/bin/sail npm install
./vendor/bin/sail npm run build

./vendor/bin/sail artisan migrate
````

Open in browser [http://localhost:8888/](http://localhost:8888/)
