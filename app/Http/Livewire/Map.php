<?php

namespace App\Http\Livewire;

use App\Events\PointAdded;
use App\Services\PointService;
use Illuminate\Database\Eloquent\Collection;
use Livewire\Component;

class Map extends Component
{
    public float $defaultLat;
    public float $defaultLng;

    public string $googleMapsApiKey;

    public Collection $points;

    public int $markerLifeTime;

    public string $coordinates;

    protected PointService $pointService;

    protected array $rules = [
        'coordinates' => [
            'required',
            'regex:/^[-]?((([0-8]?[0-9])(\.(\d{1,20}))?)|(90(\.0+)?)),(\s+)?[-]?((([0-8]?[0-9])(\.(\d{1,20}))?)|(90(\.0+)?))$/'
        ],
    ];

    protected array $messages = [
        'coordinates.regex' => 'Coordinates is not valid.',
    ];

    public function boot(
        PointService $pointService
    )
    {
        $this->pointService = $pointService;
        $this->googleMapsApiKey = config('map.api_key');
        $this->defaultLat = config('map.default_lat');
        $this->defaultLng = config('map.default_lng');
        $this->markerLifeTime = config('map.marker_lifetime');
    }

    public function mount(): void
    {
        $this->points = $this->pointService->getAllPointsForLastSeconds(
            $this->markerLifeTime
        );
    }

    public function render()
    {
        return view('livewire.map');
    }

    public function storePoint(): void
    {
        $this->validate();

        $point = $this->pointService->store([
            'lat' => explode(',', $this->coordinates)[0],
            'lng' => explode(',', $this->coordinates)[1],
        ]);

        $this->dispatchBrowserEvent('addedNewMarker', $point);
        PointAdded::dispatch($point);

        $this->coordinates = '';
    }
}
