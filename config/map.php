<?php

return [
    'api_key' => env('GOOGLE_MAPS_API_KEY', 'AIzaSyBd78D4q7ScSkUTjigntb_uahp4E3TSmCs'),

    'default_lat' => env('MAP_DEFAULT_LAT',50),

    'default_lng' => env('MAP_DEFAULT_LNG', 36),

    'marker_lifetime' => env('MAP_MARKER_LIFETIME', 60),
];
